\documentclass[english]{article}
\usepackage{geometry}
\geometry{verbose,tmargin=1in,bmargin=1in,lmargin=1in,rmargin=1in}
\usepackage{fancyhdr}
\pagestyle{fancy}
\setlength{\parskip}{\smallskipamount}
\setlength{\parindent}{0pt}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{hyperref}
\begin{document}

\graphicspath{{data/}}
\title{Changes in Temperature and Relative Humidity across Radial Distances - Microclimate in the Redwoods}


\author{Stefanus Candra\\
\texttt{candrastefan@berkeley.edu}}

\maketitle


\section{Introduction}
This report on the redwood data set aims to verify and critique Tolle et al.'s findings in their publication "A Macroscope in the Redwoods," \url{https://www.cs.duke.edu/courses/cps296.1/spring07/papers/sensys05-TollePolastreEtAl-redwoods.pdf} as well as to extract interesting information from the data set. In this paper, I will describe the steps I took in cleaning and exploring the data and present my findings on the differences in temperature and relative humidity between different radial distances from the trunk of a tree. Radial distance is the distance between the trunk and the mote. A mote with 0.1m radial distance is placed  right beside the trunk while a mote with 5m radial distance is located further away from the trunk.

\section{The Data}

\subsection{Data Collection }
The redwood data comes from two sources, \texttt{log} and \texttt{net}. The \texttt{log} data contains microclimate observations stored in the internal hard drives in the motes which are then downloaded at the end of the experiment. On the other hand, the \texttt{net} data contains observations that are transmitted to a main server via wireless network. Both data sets contain identification number of the motes (\texttt{nodeid}), time of observation (\texttt{epoch}), voltage at time of observation (\texttt{voltage}), relative humidity (\texttt{humidity}), and temperature (\texttt{humid\_temp}). There are several other columns in the data set, but they are not of interest to my findings.\\
\\
The first step is to find out whether the \texttt{log} and the \texttt{net} data are comparable. To start off, they have different number of observations with the \texttt{log} data containing about three times as many rows as the \texttt{net} data. Furthermore, there are motes in the \texttt{log} data that are not present in the \texttt{net} data and vice versa. Also, upon plotting a histogram of each variable of interest, voltage, humidity, and temperature, on each data set, we can see that there are discrepancies in the range of value of the variable.  The range of temperature, humidity, and voltage from the \texttt{net} data set are larger than those from the \texttt{log} data set.\\
\\
The smaller number of observations and higher range of values in the \texttt{net} data set can be attributed to its collection method. Wireless transmission in a grove of tall trees is likely to have many interferences, resulting in noisy observation values to be transmitted or loss of transmission altogether. This may explain why the \texttt{net} data has fewer and noisier observations.\\
\\
However, we cannot just discard the \texttt{net} data altogether because there are motes in the \texttt{net} data that do not appear in the \texttt{log} data. Furthermore, as described in the original paper "A Macroscope in the Redwoods," some motes ran out of their 512kb storage capacity and therefore, observations from later epochs only appear in the \texttt{net} data set.

\subsection{Data Cleaning}
Because we are dealing with noisy data, we have to clean it to make sure that the observations make sense before exploring it.\\
\\
First, we can see how many missing values there are in each column in both the \texttt{log} and \texttt{net} data set. Here, we are very much in luck because all nodes with missing temperature values also have missing humidity values. These are the two microclimate variables that are of interest to me, therefore I removed all rows which do not have both temperature and relative humidity.\\
\\
Next, I removed duplicate rows in each data set, in other words, rows that have the exact same values in all columns. After that, I examined rows with duplicate \texttt{(epoch, nodeid)} within each data set and found that there are small discrepancies of about 0.01 in temperature and relative humidity values between the duplicate rows. I arbitrarily decide that this scale of discrepancy is too small to affect my findings, so I randomly kept one of the duplicate rows and removed the rest.\\
\\
Afterwards, I plotted a histogram of the number of observations of each mote and made an arbitrary decision to remove observations by motes with less than 100 observations. I am making an assumption that these motes are likely to be faulty.\\
\\
To remove outliers, within each data set, I made a histogram of temperature, relative humidity, and voltage. This is how I found out that the \texttt{net} data has more variation in temperature, relative humidity and voltage than the \texttt{log} data. Then, I filtered out observations which do not make sense such as, those with relative humidity lower than 0\% as well as those with extremely low and high temperature and voltage. I kept observations with relative humidity greater than 100\% but lower than 150\% because it is possible to have supersaturation if there are dust particles flying in the air.\\
\\
Also, from doing this, we can see that voltage from the \texttt{net} data hovers around 200, while voltage from \texttt{log} data hovers around 2. It is possible that there is a difference in scale or unit between them. The relationship of voltage between the two data sets will be explored further in Section 4, Findings.

\subsection{Data Exploration}
After cleaning each data set separately, I combined them by concatenating the two data sets and removing duplicate (\texttt{epoch, nodeid}) combination. If there is a common (\texttt{epoch, nodeid}) combination in \texttt{log} and \texttt{net}, I keep the observation from the \texttt{log} data set because I think \texttt{log} data is more reliable than \texttt{net} data. Then, I joined this combined data set with \texttt{mote-location-data} table and \texttt{sonoma-dates} data to extract mote location information and observation datetime respectively.\\
\\
My aim is to find the relationship of temperature and humidity versus time across different radial distances. My hypothesis is that on average, motes with larger radial distance from the trunk will be hotter and less humid than those with smaller radial distance. To make a good comparison, I want to make sure that the only difference lies in radial distances. Therefore, the motes I am comparing should have similar height, same direction, and many overlapping epochs so that I can see the trends over many days. I narrowed down my selection to 2 motes, mote 44 with radial distance 0.1m and mote 116 with radial distance of 5m. Mote 44 is placed in WSW direction with a height of 33.5m and mote 116 is placed in WSW direction with a height of 32.6m. Figure 1 shows the plot of temperature and relative humidity over time of the two motes. The overlapping epochs are from April 28th to May 6th 2004.\\

\begin{figure}[h!]
\includegraphics[scale=0.67]{{temp.vs.time.n44.and.n116.by.Dist}.jpg}
\\
\\
\\
\includegraphics[scale=0.67]{{humid.vs.time.n44.and.n116.by.Dist}.jpg}
\caption{Plots of temperature and relative humidity versus time by radial distance from April 28th to May 6th, 2004. The mote with 0.1m radial distance is node 44 and that with 5m radial distance is node 116. They are both placed in WSW direction and have similar heights of 33.5m and 32.6m.}
\end{figure}

Looking at the plot, there does not seem to be any appreciable difference in temperature and relative humidity over time. However, when we focus on time period between 12pm and 6pm, there seems to be differences in temperature and relative humidity across radial distances. This will be explored further in Section 4, Findings.

\section{Graphical Critique}
I think Figure 3 in the original paper is successful in giving us a one dimensional analysis of the data. However, the boxplots especially in Figure 3b are difficult to read. I would replace them by plotting 3 lines, one connecting the medians, another connecting the lower quartile, and another line connecting the upper quartile. I think that will make Figure 3b graphs less cluttered and easier to read. Also, Figure 3b for Reflected PAR will benefit from having a smaller range on the vertical axis, for instance 0 to 100. Log scale might work as well.

Figure 4 gives a two dimensional analysis of the data and it does a good job in giving a snapshot of the data for that particular day. However it remains to be seen whether the same relative humidity/temperature versus time trend will be consistent from day to day. I would extend the time period to about a week to see if the trend is consistent. Also, the authors attribute the change in temperature and relative humidity to the movement of the sun and that can be verified if we see consistent trend from day to day.

Also, on the right column, the top two plots show that the top of the tree is $5^{\circ}$C warmer and 40\% less humid at that point in time. That is a very big difference and it would be interesting to see if this pattern is consistent from day to day. My guess is that this is caused by morning sunlight hitting the top of the tree, but not the bottom. Nonetheless, it is still interesting to see such dramatic microclimate difference.

\section{Findings}
Originally, I would like to compare and quantify temperature and relative humidity differences across different radial distances on average at ground level. This information is useful because people tend to avoid direct sunlight by taking shelter under a tree and I would like to know the temperature and relative humidity differences between being inside and outside the shade of a tree. I am making an assumption that having low radial distance means being more in the shade of branches and leaves, thereby avoiding direct sunlight. Following that, I am assuming that having high radial distance means being more exposed to direct sunlight.\\
\\
In the data sets, radial distances vary from 0.1m to 5m. Unfortunately, there is only one mote with 5m radial distance, which is mote 44. To make a good comparison, I select motes similar height and direction as mote 44 and unfortunately, there is only one node that fits the criteria, which is mote 116.\\
\\
The heights of the motes are about 33m which is far from ground level and it remains to be seen whether this trend at 33m also holds true at ground level. The ground is likely to have some effects on temperature and relative humidity because the soil is likely to have different temperature and moisture content than the air.\\
\\
As a bonus finding, I saw the difference in scale or unit between voltages in the \texttt{log} data and the \texttt{net} data when I was cleaning the data set and decided to see if the two are related.

\subsection{Linear Relationship between Voltages from "Log" and "Net" Data Sets}
\begin{figure}[h!]
\includegraphics[scale=0.67]{{voltage.log.net}.jpg}
\caption{Voltage from \texttt{log} versus voltage from \texttt{net}. Each point corresponds to a \texttt{(epoch, nodeid)} combination that exists in both data sets.}

\end{figure}

From the plot above, we can estimate a good linear relationship between voltages from the \texttt{log} data set and voltages from the \texttt{net} data set. The paper says that the correct voltage range is between 2.4-3.0 volts. Using this linear relationship, we can transform voltages from the \texttt{net} data set to the same unit or scale as voltages in the \texttt{log} data set which already seems to be consistent with the correct 2.4-3.0V range said in the original paper.\\
\\
With this linear transformation, we can avoid throwing away valuable data. If we just follow the original paper which says to discard observations with voltages outside the range of 2.4-3.0V, we will discard the entire \texttt{net} data which contains many observations that are not present in the \texttt{log} data.

\subsection{Temperature/RH vs Time by Radial Distance, 12pm to 6pm}
\begin{figure}[h!]
\includegraphics[scale=0.67]{{temp.vs.time.n44.and.n116.12pm.to.6pm.by.Dist}.jpg}
\\
\\
\\
\includegraphics[scale=0.67]{{humid.vs.time.n44.and.n116.12pm.to.6pm.by.Dist}.jpg}
\caption{Temperature and relative humidity versus time by radial distance from the trunk. However, only observations from 12pm to 6pm are plotted.}

\end{figure}

Focusing on time periods between 12pm to 6pm over a week, we can see in Figure 3 above that between 12pm to 6pm, the mote with 5m radial distance registers higher temperature on average than the mote with 0.1m radial distance across the week. This temperature difference is expected because motes closer to the trunk are more likely to be shaded against direct sunlight by branches and leaves. Also, temperature difference is most prominent between 12pm to 6pm probably because the sun shines brightest at this time period.\\
\\
As for relative humidity trend, lower temperature, with all other factors being equal, corresponds to higher relative humidity. However, from the graph it is difficult to see if there is any appreciable difference in relative humidity. This will be explored further in the next section.

\subsection{Temperature/RH Difference vs Time by Radial Distance, 12pm to 6pm}

To have a clearer picture on the temperature and relative humidity differences across radial distances, on Figure 4, I plotted their differences over time, i.e. values from mote 44 minus values from mote 116, and marked the $y = 0$ line to emphasize whether the differences are positive or negative.

\begin{figure}[h!]
\includegraphics[scale=0.67]{{temp.diff.vs.time.n44.and.n116.12pm.to.6pm}.jpg}
\\
\\
\\
\includegraphics[scale=0.67]{{humid.diff.vs.time.n44.and.n116.12pm.to.6pm}.jpg}

\caption{Temperature and relative humidity difference between node 44 and node 116 over time periods of 12pm to 6pm from April 28th to May 6th 2004 with horizontal line on $y = 0$.}


\end{figure}

The top plot shows that temperature differences between mote 44 with radial distance 5m and mote 116 with radial distance 0.1m are mostly positive. This is consistent with the top plot in the previous section.\\
\\
The bottom plot shows that humidity differences between mote 44 and 116 are mostly negative. We could not really see this from the bottom plot of Figure 3 in Section 4.2. Relative humidity follows a reverse trend from temperature. The mote at 0.1m radial distance has higher relative humidity than 5m radial distance and this is to be expected because lower temperature decreases the ability of air to hold moisture, resulting in higher relative humidity.

\section{Discussion and Conclusion}
These preliminary findings suggest that there are indeed temperature and relative humidity differences across radial distances from one day to another. However, it still falls short of the goal of quantifying these differences on ground level. We would like to have more than one mote for each radial distances at ground level with all other features being equal to be able to generalize the trend better. More observations on motes on different radial distances on different trees are needed.

\end{document}
