# Stat 215A, Stefanus Candra, Lab 1 script for data cleaning and preparation

# Load the libraries
library(ggplot2)
library(dplyr)
library(reshape2)

# Set the working directory to the folder containing datasets
setwd("~/Ahvaz/Stat215a//Lab1//data")

# Load the datasets
log <- read.csv('sonoma-data-log.csv', header = T)
net <- read.csv('sonoma-data-net.csv', header = T)

patient <- F

# Inspect the summaries of both log and net and see if they are comparable
# log and net are not comparable in terms of voltage and humidity
# Judgment call: use only data from log because voltage recorded is within
# the good range of 2.4V to 3.0V described in paper. Also, data from net is more
# likely to be corrupted than data from log because of wireless transmission
# in forest will have more disturbance than saving onto internal storage.
# Also, log has more observations than net

# Data cleaning
# Check for NA values
if (patient) {
  apply(log, 2, function(x) sum(is.na(x)))
  apply(net, 2, function(x) sum(is.na(x)))  
}

# log
# result_time       epoch      nodeid      parent     voltage       depth    humidity  humid_temp 
# 0           0           0           0           0           0        8270        8270 
# humid_adj     hamatop     hamabot 
# 8270        8270        8270 

# Check whether the same rows with missing values in one column are also 
# missing values in other columns
# Counts the number of rows which does not have complete data
nrow(log[!complete.cases(log), ])
nrow(net[!complete.cases(net), ])
# 8270 rows do not have complete data, consistent with the result above.
# That means these 8270 rows are missing values in all columns

# Upon inspection, these 8270 rows only have timestamps data
# Remove these 8270 rows because they only have timestamps
log <- log[complete.cases(log), ]
net <- net[complete.cases(net), ]

# Sanity check on the humidity. It cannot be below 0
nrow(filter(log, humidity < 0))
nrow(filter(net, humidity < 0))

summary(log)
summary(net)
# At this point, the humidity and temperature reading pretty much agrees with
# Table 1 of the paper.
# The result_time is all 2004-11-10 14:25:00, likely to be the time when
# the logs are downloaded

# Check the multiplicity of epoch and nodeid
group_by(log, epoch, nodeid) %>% summarise(n=n()) %>% ungroup() %>% group_by(n) %>% summarise(total=n())
group_by(net, epoch, nodeid) %>% summarise(n=n()) %>% ungroup() %>% group_by(n) %>% summarise(total=n())

# There are 8242 nodeid and epoch combination that appears twice. This shouldn't
# happen. Inspect these duplicate combinations.
# Which epoch, nodeid combinations appear twice
group_by(log, epoch, nodeid) %>% summarise(n=n()) %>% filter(n == 2)
# Makes sure that the combinations are all exact duplicates in all columns
mult <- group_by(log, epoch, nodeid) %>% summarise(n=n()) %>% filter(n == 2)
# See how many duplicates are there per nodeid
patient <- F
if (patient) {
  for (i in 1:nrow(mult)) {
    bool <- log(filter(log, epoch == mult[i, "epoch"], nodeid == mult[i, "nodeid"])[1, ] ==
      filter(log, epoch == mult[i, "epoch"], nodeid == mult[i, "nodeid"])[2, ])
    if (!bool) {
      cat("Epoch =", mult[i, "epoch"], "and nodeid = ", mult[i, "nodeid"], 
          "are not duplicates\n")
    }
  }
}
# Remove the duplicates i.e. rows with equal values in all columns
log <- log[!duplicated(log), ]
net <- net[!duplicated(net), ]
# Look at the remaining duplicate values
mult.log <- group_by(log, epoch, nodeid) %>% summarise(n=n()) %>% filter(n == 2)
mult.net <- group_by(net, epoch, nodeid) %>% summarise(n=n()) %>% filter(n == 2)
# Upon plotting the columns of some of the duplicate rows, their values only differ a little
# Arbitrarily select any one of the 2 rows with the same epoch, nodeid combination
log <- log[!duplicated(log[, c("epoch", "nodeid")]), ]
net <- net[!duplicated(net[, c("epoch", "nodeid")]), ]

# Plot the number of observations per mote
log.count <- group_by(log, nodeid) %>% summarise(n = n())
hist(log.count$n, breaks = seq(1, max(log.count$n)+100, 100))
net.count <- group_by(net, nodeid) %>% summarise(n = n())
hist(net.count$n, breaks = seq(1, max(net.count$n)+100, 100))
# Remove motes with less than 100 observations
log <- anti_join(log, filter(log.count, n < 100), by = "nodeid")
net <- anti_join(net, filter(net.count, n < 100), by = "nodeid")

# When the humidity is below 0, temperature shows -38.4 which is outside
# a range of 6.6 and 32.6. This indicates abnormal reading.
# According to the paper, humidity ranges from 16.4 to 100.2
hist(log$humidity)
log <- filter(log, humidity > 0, humidity < 150)
hist(net$humidity)
net <- filter(net, humidity > 0, humidity < 150)

# Filter voltage to remove the outlier
hist(log$voltage)
log <- filter(log, voltage > 2.0)
hist(net$voltage)
net <- filter(net, voltage < 400)

# Filter temperature outlier
hist(log$humid_temp)
hist(net$humid_temp)
net <- filter(net, humid_temp < 50)

# Check again that the epoch, nodeid combination is unique throughout the dataframe
group_by(log, epoch, nodeid) %>% summarise(n=n()) %>% ungroup() %>% 
  group_by(n) %>% summarise(total=n())
# n = 1, total = 250233, no more epoch, nodeid duplicate
group_by(net, epoch, nodeid) %>% summarise(n=n()) %>% ungroup() %>% 
  group_by(n) %>% summarise(total=n())

# Select the columns we are interested in (epoch, nodeid, voltage, depth, humidity
# , humid_temp, humid_adj) and remove the result_time because the values are wrong.
log <- select(log, epoch, nodeid, voltage, humidity, 
                            humid_temp, humid_adj)
net <- select(net, epoch, nodeid, voltage, humidity, 
                            humid_temp, humid_adj)

# Examine voltage relationship between net and log
voltage.log <- select(log, epoch, nodeid, voltage)
colnames(voltage.log) <- c("epoch", "nodeid", "log.voltage")
voltage.net <- select(net, epoch, nodeid, voltage)
colnames(voltage.net) <- c("epoch", "nodeid", "net.voltage")

# Get the intersection of voltage log and net in terms of epoch and nodeid
voltage.intersect <- inner_join(voltage.log, voltage.net, by = c("epoch", "nodeid"))
# Plot voltage.net vs voltage.log
qplot(data = voltage.intersect, y = log.voltage, x = net.voltage) +
  stat_smooth(method = "lm", formula = y~x) +
  xlab("Voltage from 'net' data") +
  ylab("Voltage from 'log' data") + 
  ggtitle("Voltage from 'Log' vs Voltage from 'Net'") +
  theme(plot.title = element_text(vjust = 2)) +
  theme(axis.title.x = element_text(vjust = -0.5)) + 
  theme(axis.title.y = element_text(vjust = 1.2))
ggsave(filename = "voltage.log.net.jpg")
# There is a linear relationship between voltage.net and voltage.log
voltage.lm <- lm(voltage.intersect$log.voltage ~ voltage.intersect$net.voltage)
# Transform voltage net to the scale of voltage log
net$voltage <- net$voltage*voltage.lm$coefficients[2] + voltage.lm$coefficients[1]

# Examine temperature relationship between net and log
temp.log <- select(log, epoch, nodeid, humid_temp)
colnames(temp.log) <- c("epoch", "nodeid", "log.temp")
temp.net <- select(net, epoch, nodeid, humid_temp)
colnames(temp.net) <- c("epoch", "nodeid", "net.temp")
temp.intersect <- inner_join(temp.log, temp.net, by = c("epoch", "nodeid"))
qplot(data = temp.intersect, y = log.temp, x = net.temp)
# No transformation needed

# Combine net and log, if there is duplicate epoch, nodeid combination take the row from log
not.in.log <- anti_join(net, log, by = c("epoch", "nodeid"))
all <- rbind(log, not.in.log)

# Check the multiplicity of node and epoch
group_by(all, nodeid, epoch) %>% summarise(n = n()) %>% ungroup() %>% group_by(n) %>% summarise(total = n())

# Save the file
data <- all

# Combine with other datasets
locs <- read.table('mote-location-data.txt', header = T)
epoch.dates <- scan(file = "epochDates.txt", what = "", sep = ",")

# Convert epoch dates to time
epoch.dates <- as.POSIXct(epoch.dates, format = "%a %b %d %H:%M:%S %Y")

# Epoch dates already mapped to epoch number
# Create a data frame with epoch number and epoch time as columns
epoch <- c(1:length(epoch.dates))
epoch.time <- data.frame(epoch, epoch.datetime = epoch.dates)

# Join epoch dates with main data table by epoch number
data <- inner_join(data, epoch.time, by = "epoch")

# Left join main data table with locs by ID because there is a node with no
# location information and we want to preserve it.
names(locs)[names(locs) == "ID"] <- "nodeid"
data <- left_join(data, locs, by = "nodeid")

# Drop the Tree column because we don't need it
data <- select(data, -Tree)
save(data, file = "cleaned_data.Rda")